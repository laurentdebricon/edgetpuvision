#!/bin/bash

wget -q --spider http://google.com
if [ $? -eq 0 ]; then
    echo "Online"
	cd /usr/lib/python3/dist-packages/edgetpuvision/
	git pull
	cd script
	./update.sh
else
    echo "Offline"
fi

#edgetpu_detect --model /usr/lib/python3/dist-packages/edgetpuvision/model/masks.tflite --labels /usr/lib/python3/dist-packages/edgetpuvision/model/masks.txt --threshold 0.60 --source /dev/video1:YUY2:864x480:24/1

edgetpu_detect --model /usr/lib/python3/dist-packages/edgetpuvision/model/masks.tflite --labels /usr/lib/python3/dist-packages/edgetpuvision/model/masks.txt --threshold 0.60 --source /dev/video1:jpeg:1280x720:30/1
