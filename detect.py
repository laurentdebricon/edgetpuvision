# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A demo which runs object detection on camera frames.

export TEST_DATA=/usr/lib/python3/dist-packages/edgetpu/test_data

Run face detection model:
python3 -m edgetpuvision.detect \
  --model ${TEST_DATA}/mobilenet_ssd_v2_face_quant_postprocess_edgetpu.tflite

Run coco model:
python3 -m edgetpuvision.detect \
  --model ${TEST_DATA}/mobilenet_ssd_v2_coco_quant_postprocess_edgetpu.tflite \
  --labels ${TEST_DATA}/coco_labels.txt
"""

import argparse
import collections
import colorsys
import itertools
import svgwrite
import numpy as np
import time

import os
from urllib.request import pathname2url
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst

Gst.init(None)


from .centroidtracker import CentroidTracker
from threading import Thread

from edgetpu.detection.engine import DetectionEngine

from . import svg
from . import utils
from .apps import run_app


# https://gist.github.com/don1138/5761014
# System 
font_family_system = 'font-family: system, -apple-system, ".SFNSText-Regular", "San Francisco", "Roboto", "Segoe UI", "Helvetica Neue", "Lucida Grande", sans-serif'

# Times New Roman-based serif
font_family_times = 'font-family: Cambria, "Hoefler Text", Utopia, "Liberation Serif", "Nimbus Roman No9 L Regular", Times, "Times New Roman", serif'

# A modern Georgia-based serif 
font_family_georgia = 'font-family: Constantia, "Lucida Bright", Lucidabright, "Lucida Serif", Lucida, "DejaVu Serif", "Bitstream Vera Serif", "Liberation Serif", Georgia, serif';

# A more traditional Garamond-based serif 
font_family_garamond = 'font-family: "Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif';

# The Helvetica/Arial-based sans serif 
font_family_helvetica = 'font-family: Frutiger, "Frutiger Linotype", Univers, Calibri, "Gill Sans", "Gill Sans MT", "Myriad Pro", Myriad, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif'

# The Verdana-based sans serif
font_family_verdana = 'font-family: Corbel, "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", "Bitstream Vera Sans", "Liberation Sans", Verdana, "Verdana Ref", sans-serif'

# The Trebuchet-based sans serif
font_family_trebuchet = 'font-family: "Segoe UI", Candara, "Bitstream Vera Sans", "DejaVu Sans", "Bitstream Vera Sans", "Trebuchet MS", Verdana, "Verdana Ref", sans-serif'

# The heavier "Impact" sans serif
font_family_impact = 'font-family: Impact, Haettenschweiler, "Franklin Gothic Bold", Charcoal, "Helvetica Inserat", "Bitstream Vera Sans Bold", "Arial Black", sans-serif'

# The monospace
font_family_monospace = 'font-family: Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace'

color_red_covid = 'rgb(237, 28, 36)'
color_blue_covid = 'rgb(40, 58, 127)'
play_sound_position = 1
play_last_time_timestamp = 0
playbin = 0

BBox = collections.namedtuple('BBox', ('x', 'y', 'w', 'h'))
BBox.area = lambda self: self.w * self.h
BBox.scale = lambda self, sx, sy: BBox(x=self.x * sx, y=self.y * sy,
                                       w=self.w * sx, h=self.h * sy)
BBox.__str__ = lambda self: 'BBox(x=%.2f y=%.2f w=%.2f h=%.2f)' % self

Object = collections.namedtuple('Object', ('id', 'label', 'score', 'bbox'))
Object.__str__ = lambda self: 'Object(id=%d, label=%s, score=%.2f, %s)' % self

def size_em(length):
    return '%sem' % str(1 * (length))

def color(i, total):
    return tuple(int(255.0 * c) for c in colorsys.hsv_to_rgb(i / total, 1.0, 1.0))

def make_palette(keys):
    return {key : svg.rgb(color(i, len(keys))) for i, key in enumerate(keys)}

def make_get_color(color, labels):
    if color:
        return lambda obj_id: color

    if labels:
        palette = make_palette(labels.keys())
        return lambda obj_id: palette[obj_id]

    return lambda obj_id: 'white'



def shadow_text(dwg, x, y, text, font_size=10):
    dwg.add(dwg.text(text, insert=(x + 1, y + 1), fill='black',
                     font_size=font_size, style=font_family_trebuchet))
    dwg.add(dwg.text(text, insert=(x, y), fill='white',
                     font_size=font_size, style=font_family_trebuchet))



def overlay(title, objs, get_color, inference_time, inference_rate, layout, objects):
    x0, y0, width, height = layout.window
    # print(layout)

    dwg = svgwrite.Drawing('', size=(width, height), viewBox=('%s %s %s %s' % layout.window)) # , debug=True sert à rien car fucked callstack

    text_fill_color = 'rgb(40, 58, 127)'
    for obj in objs:
        percent = int(100 * obj.score)
        width_rect = False
        if obj.label == "KO":
            caption = '%s' % ("Portez votre MASQUE !")
            text_fill_color = 'white'
            color = color_red_covid
            width_rect = '19.4em'
        elif obj.label == "OK":
            caption = '%s' % (obj.label)
            text_fill_color = 'rgb(40, 58, 127)'
            color = "rgb(0, 255, 0)"
            width_rect = '2.9em'
        elif obj.label:
            caption = '%d%% %s' % (percent, obj.label)
            color = get_color(obj.id)
        else:
            caption = '%d%%' % percent
            color = get_color(obj.id)

        if not width_rect:
            width_rect = size_em(len(caption))
            
        # layout.size est faux !! mais ça marche bien avec viewbox
        x, y, w, h = obj.bbox.scale(*layout.size)
            
        dwg.add(dwg.rect(insert=(x, y), size=(w, h), stroke=color, stroke_width=3, fill='none'))
        dwg.add(dwg.rect(insert=(x, y+h), size=(width_rect, '1.4em'), stroke=color, stroke_width=2, fill=color))        
        dwg.add(dwg.text(caption, insert=(x+2, y+h+15), fill=text_fill_color, fill_opacity="1",  style=font_family_verdana, font_size="20"))

    # for (objectID, centroid) in objects.items():
    #     text = "{}".format(objectID)
    #     dwg.add(dwg.text(text, insert=(centroid[0] - 10, centroid[1] - 10), fill='white', fill_opacity="1",  style=font_family_verdana))
    
       

    # Info
    line = 'Inference time: %.2f ms (%.2f fps) -- Objects: %d' % (inference_time * 1000, 1.0 / inference_time, len(objs))
    #shadow_text(dwg, 2, height - 2, line)
    
    ### FOR SVG COLORS, ONLY NAMES (black, white) OR rgb() , no HEX !!!!
    g1 = dwg.add(dwg.g(id='covid', transform="translate(%d %d)" % (width-100, y0)))
    g1.add(dwg.rect(insert=(0, 0), size=(100, 44), fill='rgb(40, 58, 127)'))
    g1.add(dwg.text("COVID-19", insert=(9,28), fill='white',font_size=17, style=font_family_trebuchet))
    
    shadow_text(dwg, 10, 550, "Non connecté à l'internet", 15)
    shadow_text(dwg, 10, 567, "Aucune collecte de donnée", 15)
    
   # dwg.add(dwg.text("Non connecté à l'internet, Aucune collecte de donnée", insert=(10, height-10), fill='white', fill_opacity="1", style=font_family_verdana, transform="rotate(-90)"))

    
    # gdata = dwg.add(dwg.g(id='data', transform="translate(%d %d)" % (width-200, y0+200)))
    # g1.add(dwg.rect(insert=(0, 0), size=(100, 44), fill='rgb(40, 58, 127)'))
    # g1.add(dwg.text("COVID-19", insert=(9,28), fill='white',font_size=17, style=font_family_trebuchet))

    g2 = dwg.add(dwg.g(transform="translate(%d 6)" % (width/2)))
    g2.add(dwg.text("ICI, LE MASQUE EST OBLIGATOIRE", insert=(0,y0+32), fill=color_blue_covid, font_weight='bold', font_size=45, style=font_family_trebuchet, text_anchor="middle", stroke="black", stroke_opacity="0.3", stroke_width="1px"))

    g2.add(dwg.path(id='bulle', d='m 0,2.7666601 h 191.60001 l -11,31.8999999 44.8,-31.9999999 h 224.4', fill='none', stroke_width="5.333", stroke=color_red_covid, transform="translate(0 40)"))

    g3 = dwg.add(dwg.g(id='pictoHomme', transform="translate(0 %d) scale(0.4 0.34)" % (y0), stroke="rgb(40, 58, 127)"))
    
    g3.add(dwg.path(id='cercle', d='m 278.122,649.28481 c 111.46639,0 192.26616,-90.66644 192.26616,-202.26616 0,-111.46639 -90.66644,-202.13283 -202.26616,-202.13283 -111.46639,0 -192.132825,90.66644 -192.132825,202.13283 0,111.46639 90.666435,202.26616 202.132825,202.26616 z', fill='white', fill_opacity='0.9' ,stroke_width="14.4"))

    g3.add(dwg.path(id='mask', d='m 189.3222,417.55206 c 0,0 3.19999,8.26664 20.66661,12.26663 l -1.33333,-8.93331 c 0,0 45.19989,-9.46664 65.33317,-8.53331 19.73328,0.8 63.33318,9.06664 63.33318,9.06664 l -2.93333,10.53331 c 0,0 13.3333,-1.99999 18.26662,-13.99996 l 2,1.73333 c 0,0 -3.86666,12.79996 -19.86662,15.33329 l -1.06667,40.53323 -6.53331,20.66662 c 0,0 7.33331,-4.39999 10.13331,-8.66665 l -2.26667,7.19998 -11.19997,12.13331 c 0,0 -26.53326,29.59992 -50.13321,29.06659 -41.33323,-0.66666 -57.86652,-33.33325 -57.86652,-33.33325 0,0 -11.46664,-12.39997 -13.3333,-15.46663 l -0.93333,-4.66665 c 0,0 10.13331,10.26664 13.3333,10.26664 0.8,-0.66667 -3.59999,-15.59996 -3.59999,-15.59996 l -0.93333,-44.13322 c 0,0 -18.53329,-4.26666 -21.59995,-11.06664 z' , fill='rgb(63, 95, 181)', stroke_linecap="round",stroke_linejoin="round",stroke_width="3.333"))


    g3.add(dwg.path(id='bordsvisages', d='m 183.18888,409.28541 c 0,0 -5.33332,-1.59999 -7.46665,6.39999 -2.13333,7.99998 4.13332,30.93325 7.73331,39.1999 3.59999,8.26664 4.66666,20.66661 14.1333,18.66662 l 10.26664,25.19993 c 0,0 5.59999,29.0666 0.66667,41.46657 -5.81665,11.70383 -6.31721,18.35907 -14.50405,28.54677 16.9818,21.95369 36.78186,38.39301 84.24223,33.43746 26.41764,-4.77115 41.2231,-7.77062 48.95628,-24.11274 0,0 -9.89473,-20.00487 -5.76141,-42.93815 4.26665,-22.93327 7.59998,-35.33324 7.59998,-35.33324 0,0 10.66664,-9.73331 14.39996,-24.93327 0,0 4.13333,1.86666 7.59998,-2.93333 3.33333,-4.79999 9.06665,-20.66661 10.26665,-23.99994 1.33333,-3.46666 11.19997,-25.99993 6.13331,-33.33325 -5.06665,-7.33331 -10.66664,-4.39999 -10.66664,-4.39999 l -6.66665,-52.26653 -31.33325,-43.06656 -57.99986,-11.73331 -25.0666,12.66664 -20.26662,8.53331 -20.39995,27.4666 -1.73332,37.73324 z', fill='none', stroke_linecap="round" ,stroke_width="3.333"))


    g3.add(dwg.path(id='tshirtbords', d='m 194.0181,568.76519 c 23.05789,32.57357 58.34372,39.08283 89.26566,32.20246 14.19953,-3.1595 36.73836,-4.53136 44.57142,-25.14932 0,0 50.3211,8.83508 69.38772,24.03504 -70.57499,67.03862 -190.55198,59.78311 -260.99188,-14.63132 26.13327,-12.39997 32.67123,-10.60372 42.80453,-19.13703 10.13331,-8.39998 14.96255,2.68017 14.96255,2.68017 z', fill='rgb(63, 95, 181)', stroke_linecap="round", stroke_width="3.333"))


    g3.add(dwg.path(id='cheveux', d='m 191.18886,439.01867 5.33332,-51.19987 c 0,0 18.13329,-56.66653 33.99991,-57.73319 15.7333,-1.06667 31.19992,-1.33333 44.79989,-2.93333 13.7333,-1.73333 28.93326,-3.33332 39.86657,4.66666 10.9333,7.99998 30.39992,47.86655 31.86658,62.39984 1.33333,14.5333 1.33333,47.59988 1.33333,47.59988 0,0 3.99999,-25.4666 11.06664,-32.26658 0,0 21.46662,-36.39991 9.06665,-67.19983 -8.5701,-20.78266 -23.69196,-38.19855 -43.06656,-49.59988 -3.06666,-0.53333 -13.3333,-27.0666 -53.86653,-24.66661 -40.66657,2.66666 -82.26646,27.4666 -92.9331,65.19984 -10.66664,37.73324 4.39998,77.73314 4.39998,77.73314 z m 0,0 5.33332,-51.19987 c 0,0 18.13329,-56.66653 33.99991,-57.73319 15.7333,-1.06667 31.19992,-1.33333 44.79989,-2.93333 13.7333,-1.73333 28.93326,-3.33332 39.86657,4.66666 10.9333,7.99998 30.39992,47.86655 31.86658,62.39984 1.33333,14.5333 1.33333,47.59988 1.33333,47.59988 0,0 3.99999,-25.4666 11.06664,-32.26658 0,0 21.46662,-36.39991 9.06665,-67.19983 -8.5701,-20.78266 -23.69196,-38.19855 -43.06656,-49.59988 -3.06666,-0.53333 -13.3333,-27.0666 -53.86653,-24.66661 -40.66657,2.66666 -82.26646,27.4666 -92.9331,65.19984 -10.66664,37.73324 4.39998,77.73314 4.39998,77.73314 z', fill='rgb(63, 95, 181)',stroke_width="4"))

    g4 = dwg.add(dwg.g(transform="translate(%d %d)" % (width/2, y0+height-10)))
    g4.add(dwg.text("Ensemble, faisons bloc contre le Coronavirus", insert=(0,0), fill='white', font_weight='bold', font_size=40, stroke="black", text_anchor="middle", stroke_opacity="0.3", stroke_width="1px", style=font_family_trebuchet))   
   
    #print(dwg.tostring())

    return dwg.tostring()


def convert(obj, labels):
    x0, y0, x1, y1 = obj.bounding_box.flatten().tolist()
    return Object(id=obj.label_id,
                  label=labels[obj.label_id] if labels else None,
                  score=obj.score,
                  bbox=BBox(x=x0, y=y0, w=x1 - x0, h=y1 - y0))

def print_results(inference_rate, objs):
    print('\nInference (rate=%.2f fps):' % inference_rate)
    for i, obj in enumerate(objs):
        print('    %d: %s, area=%.2f' % (i, obj, obj.bbox.area()))


# compare the co-ordinates for dictionaries of interest
def DictDiff(dict1, dict2):
    dict3 = {**dict1}
    for key, value in dict3.items():
        if key in dict1 and key in dict2:
            dict3[key] = np.subtract(dict2[key], dict1[key])
    return dict3

def playsoundNix(sound):  
    global playbin 
    playbin.set_property("uri", "file://" + pathname2url(os.path.abspath(sound)))
    set_result = playbin.set_state(Gst.State.PLAYING)

    # if set_result != Gst.StateChangeReturn.ASYNC:
    #     raise PlaysoundException(
    #         "playbin.set_state returned " + repr(set_result))


def on_message(bus, message):
    t = message.type
    if t == Gst.MessageType.EOS:
        #print(t)
        # il faut bien avoir des wav de plus d'1 secondes sinon ça coupe avant
        playbin.set_state(Gst.State.NULL)
    elif t == Gst.MessageType.ERROR:
        playbin.set_state(Gst.State.NULL)
        err, debug = message.parse_error()
        print ("Error: %s" % err, debug)

def sound_warning(force=False):
    global play_last_time_timestamp
    global play_sound_position
    
    now = round(time.time())

    if(force or now > play_last_time_timestamp+10):
        play_last_time_timestamp = now 
        sound = '/usr/lib/python3/dist-packages/edgetpuvision/sound/caroline/%d.wav' % play_sound_position
        play_sound_position = play_sound_position + 1
        if play_sound_position == 11:
            play_sound_position = 1
        playsoundNix(sound)
        return True
    else:
        #print("DECLENCHEMENT TROP TOT D'UN SON")
        return False


def render_gen(args):
    global playbin
    fps_counter  = utils.avg_fps_counter(30)

    engines, titles = utils.make_engines(args.model, DetectionEngine)
    assert utils.same_input_image_sizes(engines)
    engines = itertools.cycle(engines)
    
    engine = next(engines)

    labels = utils.load_labels(args.labels) if args.labels else None
    filtered_labels = set(l.strip() for l in args.filter.split(',')) if args.filter else None
    get_color = make_get_color(args.color, labels)

    draw_overlay = True

    yield utils.input_image_size(engine)
    
    obsFrames = 0 # tracking inspired by https://github.com/Tqualizer/Directional-object-tracking-with-TFLite-and-Edge-TPU/
    ct = CentroidTracker(maxDisappeared=15, maxDistance=10000)
    # je dois baisser maxDisappeared car je calcule plus a toutes les frames
    objects ={}
    old_objects={}
    warned_id = []
    playbin = Gst.ElementFactory.make('playbin', 'playbin')
    bus = playbin.get_bus()
    bus.add_signal_watch()
    bus.connect("message", on_message)

    output = None
    while True:
        tensor, layout, command = (yield output)

        inference_rate = next(fps_counter)

        
        if draw_overlay:
            start = time.monotonic()
            objs = engine.detect_with_input_tensor(tensor, threshold=args.threshold, top_k=args.top_k)
            inference_time = time.monotonic() - start
            objs = [convert(obj, labels) for obj in objs]

            if labels and filtered_labels:
                objs = [obj for obj in objs if obj.label in filtered_labels]

            objs = [obj for obj in objs if args.min_area <= obj.bbox.area() <= args.max_area]
        
            # On the next loop set the value of these objects as old for comparison
            if obsFrames % 5 == 0:
                old_objects.update(objects)                  
                rects = [];
                
                someoneIsNotWearingMask = False
                if objs:
                    for obj in objs:
                        x, y, w, h = tuple(obj.bbox.scale(*layout.size));
                        rects.append((x, y, x+w, y+h))
                        if obj.label == 'KO':
                            someoneIsNotWearingMask = True
                        
                #update the centroid for the objects
                objects = ct.update(rects)
                # calculate the difference between this and the previous frame

             #see what the difference in centroids is after every x frames to determine direction of movement
    #and tally up total number of objects that travelled left or right
            #print(obsFrames)
            

            if obsFrames % 5 == 0:  # je veux une détection très rapide :P
                d = []            
                for k,v in objects.items():
                    d.append(k)
                if bool(d):                           
                    if someoneIsNotWearingMask:
                        needToWarn = False
                        for id in d:   # je warn qu'une fois la même personne
                            if(id not in warned_id):
                                needToWarn = True  
                        if needToWarn: #j'émet qu'une fois meme si plusieurs ont pas leur masque   
                            success = sound_warning()
                            if success: # car le son a pas toujours lieu si ça faisait pas longtemps
                                for id in d:   # je warn qu'une fois la même personne
                                    if(id not in warned_id):
                                        warned_id.append(id)

                            

            if obsFrames % 30 == 0:                
                pixelDiffs = DictDiff(objects,old_objects)
                d = {}
                for k,v in pixelDiffs.items():
                    if v[0] > 3: 
                        d[k] =  "Left"
                        #leftcount = leftcount + 1 
                    elif v[0]< -3:
                        d[k] =  "Right"
                        #rightcount = rightcount + 1 
                    elif v[1]> 3:
                        d[k] =  "Up"
                    elif v[1]< -3:
                        d[k] =  "Down"
                    else: 
                        d[k] = "Stationary"
                if bool(d):                           
                    print(d, time.ctime()) # prints the direction of travel (if any) and timestamp

            if obsFrames % 3000 == 0:
                warned_id = [] # garbage collection
            
            obsFrames = obsFrames + 1


            if args.print:
                print_results(inference_rate, objs)

            title = titles[engine]
            output = overlay(title, objs, get_color, inference_time, inference_rate, layout, objects)
        else:
            output = None

        if command == 'o':
            draw_overlay = not draw_overlay
        elif command == 'n':
            engine = next(engines)

def add_render_gen_args(parser):
    parser.add_argument('--model',
                        help='.tflite model path', required=True)
    parser.add_argument('--labels',
                        help='labels file path')
    parser.add_argument('--top_k', type=int, default=50,
                        help='Max number of objects to detect')
    parser.add_argument('--threshold', type=float, default=0.1,
                        help='Detection threshold')
    parser.add_argument('--min_area', type=float, default=0.0,
                        help='Min bounding box area')
    parser.add_argument('--max_area', type=float, default=1.0,
                        help='Max bounding box area')
    parser.add_argument('--filter', default=None,
                        help='Comma-separated list of allowed labels')
    parser.add_argument('--color', default=None,
                        help='Bounding box display color'),
    parser.add_argument('--print', default=False, action='store_true',
                        help='Print inference results')

    #TODO find a way to pass this arg to gl video flip
    #parser.add_argument('--mirror', help='flip video horizontally', action='store_true')

def main():
    run_app(add_render_gen_args, render_gen)

if __name__ == '__main__':
    main()
