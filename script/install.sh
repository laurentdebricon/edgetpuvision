cd /usr/lib/python3/dist-packages
sudo mv edgetpuvision edgetpuvision.old

cd ~
sudo apt-get install git python3-scipy


#https://serverfault.com/questions/447028/non-interactive-git-clone-ssh-fingerprint-prompt
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null;

git clone https://bitbucket.org/laurentdebricon/edgetpuvision # public repo


# https://github.com/google-coral/edgetpu/issues/120
sudo ln -s /home/mendel/edgetpuvision /usr/lib/python3/dist-packages/edgetpuvision
# sudo systemctl enable /home/mendel/edgetpuvision/script/detects.service
# la méthode avec le lien symbolique d'un service fonctionne pas avec systemctl :(
# même avec ces astuces au reboot ça oublie tout :(
sudo cp /home/mendel/edgetpuvision/script/detects.service /lib/systemd/system/detects.service 
sudo systemctl enable detects.service

sudo cp edgetpuvision/script/boot.scr /boot/
# hide kernel boot log

sudo mv /usr/share/weston/background.png /usr/share/weston/background.png.old
sudo ln -s /home/mendel/edgetpuvision/img/background.png /usr/share/weston/background.png 


cd /home/mendel/edgetpuvision/
pip3 install svgwrite